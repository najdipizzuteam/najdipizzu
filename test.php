<?php

    require_once 'lib/pharse.php';
    require_once 'entity/Pizza.php';
    require_once 'util/StringUtils.php';
    require_once 'util/db.php';

    $html = Pharse::file_get_dom('http://brandys.turbopizza.cz/nabidka.html');

    $pizzadivs = $html->select(".pizza_line");

    foreach ($pizzadivs as $pizzadiv) {
        $nazev = $pizzadiv->select("h2", 0)->getPlainText();
        $ingredience = $pizzadiv->select("p", 0)->getPlainText();
        $cena = $pizzadiv->select('span', 0)->getPlainText();

        $ingarr = splitIngredients($ingredience);

        $pizzaArr[] = new Pizza($nazev, $cena, $ingarr);

    }

?>