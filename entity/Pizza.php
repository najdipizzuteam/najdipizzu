<?php
/**
 * Created by PhpStorm.
 * User: iJuhan
 * Date: 21.09.2018
 * Time: 20:13
 */

class Pizza
{

    var $name; //Jméno pizzy
    var $pizzeria_ids; //Array s ID pizzérií, které mají tuhle pizzu
    var $price; //Cena
    var $ingredients; //Array s ingrediencemi


    /**
     * @param mixed $name
     */
    function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $price
     */
    function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    function getPrice()
    {
        return $this->price;
    }

    function __construct($pizzaName, $price, $ingredients)
    {
        $this->name = $pizzaName;
        $this->$price = $price;
        $this->ingredients = $ingredients;
    }


}